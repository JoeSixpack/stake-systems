#!/usr/bin/env bash
set -exo pipefail

mkdir -p "${HOME}/.local/bin"

# installing git and ansible
sudo apt-get update
sudo apt-get install -y git ansible

# cloning repository
BASE_DIR="${HOME}/${STASYS_REPO##*/}"
git clone "${STASYS_REPO}" "${BASE_DIR}" || true
cd "${BASE_DIR}"
git pull && git reset HEAD --hard && git clean -xdf

cd ./ansible
ansible-playbook ./base.yml

# npm global environment
mkdir -p "${HOME}/.npm-global"
npm config set prefix "${HOME}/.npm-global"

# oh-my-zsh
cd "${BASE_DIR}"
./ubuntu/omz.sh

# .zprofile
cat << "EOF" > "${HOME}/.zprofile"
PATH="${HOME}/.local/bin:${PATH}"
PATH="${HOME}/.npm-global/bin:${PATH}"
export PATH
EOF

# ssh-keygen
cd "${BASE_DIR}"
./ubuntu/ssh-keygen.sh
