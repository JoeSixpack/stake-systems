# Tendermint on Docker

## requirements

* `bash` `docker.io` `jq` `bc`

## usage

``` bash
usage: ./validator.sh subcommand [options] [args]

subcommands

  set      - set the current validator network
  build    - builds the container
  run      - run commands from within the container
  start    - starts the container
  logs     - tails container logs
  exec     - run commands inside the running container
  status   - get container status
  identity - shows/sets priv_validator_key.json from identity/*.json
    list   - shows identity/*.json
    save   - saves priv_validator_key.json to identity/*.json
    remove - removes identity from identity/*.json
    rename - rename identity from identity/*.json
    scp    - scp identity to a remote machine via ssh
  rsync    - rsync validator data to a remote machine via ssh
  stop     - stops the container
  remove   - removes the container
  restart  - restarts the container
  update   - updates the running container
  clean    - clean unused docker images and volumes
  purge    - stop all running containers, purge everything
```

## setting up a gaia-13007 node

the following setup is chain specific, use the developer's README

``` bash
# setting up the environment
./validator.sh set gaia-13007

# building a new validator
./validator.sh build

# initializing the validator
./validator.sh run gaiad init rottentomatoes --chain-id gaia-13007

# adding a wallet
./validator.sh run gaiacli keys add testnet-wallet # --recover

# listing wallets
./validator.sh run gaiacli keys list

# fetching genesis from github repo
./validator.sh run fetch_genesis https://raw.githubusercontent.com/cosmos/testnets/master/gaia-13k/13007/genesis.json

# starting the validator
./validator.sh start

# get status
./validator.sh status

# checking the logs
./validator.sh logs

# you need to wait for the full chain to be synchronized

# creating the validator transaction
# first, we need to get into running container for this to work
./validator.sh exec bash

# note - run vs. exec
# run starts a new container, executes the code, then deletes the container
# exec attaches to the existing container
# run is the desired behavior when the validator is not yet started
# exec for everything else

# from the running container, run
gaiacli tx staking create-validator \
  --amount=1000000umuon \
  --pubkey="$(gaiad tendermint show-validator)" \
  --moniker=rottentomatoes \
  --chain-id=gaia-13007 \
  --from=testnet-wallet \
  --commission-rate="0.1" \
  --commission-max-rate="0.20" \
  --commission-max-change-rate="0.01" \
  --min-self-delegation=1000000

# updating the validator after a change in the configuration file
./validator.sh update

# stopping the validator
./validator.sh stop

# removing the validator
./validator.sh remove

# cleanup - unused images and volumes
./validator.sh clean

# purge - delete every container, image and volume used by docker
./validator.sh purge
```

## additional info

* use `./validator.sh status` to get some nice stats

``` bash
$ ./validator.sh set kava-testnet-4000
$ ./validator.sh status
chain       : kava-testnet-4000
healthcheck : healthy
image       : f3e5adeeb238
container   : f151261afac0
voting_power: 0
catchup     : false
latest_block: 6 seconds ago
usage_mb    : 15352 /home/user/tendermint-1/kava-testnet-4000
priv_json   : /home/user/tendermint-1/kava-testnet-4000/identity/random.json
```

* always refer to the gaia-13007 testnet file for help `network/gaia-13007.env`  

* default persistence directory is at `${HOME}/tendermint-1`  
in the current set, if your username is joe, the persistent data for gaia is at  
`/home/joe/tendermint-1/gaia-13007`
