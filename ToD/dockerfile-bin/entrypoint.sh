#!/usr/bin/env bash
set -eEuo pipefail
trap 'RC=$?; echo [error] exit code $RC running $BASH_COMMAND; exit $RC' ERR

# setting permissions
chown "${TENDERMINT_USER}":"${TENDERMINT_USER}" "${USER_HOME}/.${SERVER_BIN}"
chown "${TENDERMINT_USER}":"${TENDERMINT_USER}" "${USER_HOME}/.${CLI_BIN}"

# setting up config file
if [ -f "${USER_HOME}/.${SERVER_BIN}/config/config.toml" ]; then
  sed -E -i "s|laddr = \"tcp://127.0.0.1:26657\"|laddr = \"${RPC_LADDR}\"|" "${USER_HOME}/.${SERVER_BIN}/config/config.toml"
  sed -E -i "s|seeds = \".*\"|seeds = \"${SEEDS}\"|" "${USER_HOME}/.${SERVER_BIN}/config/config.toml"
  sed -E -i "s|persistent_peers = \".*\"|persistent_peers = \"${PEERS}\"|" "${USER_HOME}/.${SERVER_BIN}/config/config.toml"  
fi

case "${1}" in
  fetch_genesis)
    shift
    curl --fail -sSL "${1}" -o "${USER_HOME}/.${SERVER_BIN}/config/genesis.json"
    chown "${TENDERMINT_USER}":"${TENDERMINT_USER}" "${USER_HOME}/.${SERVER_BIN}/config/genesis.json"
  ;;
  *)
    exec gosu "${TENDERMINT_USER}" "$@"
  ;;
esac