ARG GO_VERSION
FROM golang:$GO_VERSION

RUN export DEBIAN_FRONTEND=noninteractive \
  && apt-get -q update \
  && apt-get -q -y install --no-install-recommends \
    build-essential \
    curl \
    gosu \
    htop \
    jq \
    mc \
    nano \
    netcat \
    net-tools \
    tini \
    tcpdump

RUN curl -LSsf https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

ARG TENDERMINT_USER
ARG CHAIN_ID
ARG REPO
ARG CHECKOUT
ARG MAKE_TARGETS
ARG SERVER_BIN
ARG CLI_BIN

RUN useradd -m -d "/home/${TENDERMINT_USER}" -s /bin/bash "${TENDERMINT_USER}" \
  && export GOBIN="${GOPATH}/bin" \
  && curl --fail -sSL "${REPO}/${SERVER_BIN}" -o "${GOBIN}/${SERVER_BIN}" \
  && chmod +x "${GOBIN}/${SERVER_BIN}" \
  && curl --fail -sSL "${REPO}/${CLI_BIN}" -o "${GOBIN}/${CLI_BIN}" \
  && chmod +x "${GOBIN}/${CLI_BIN}" \
  && "${SERVER_BIN}" version \
  && "${CLI_BIN}" version

RUN du -sm "${GOPATH}/src"

COPY entrypoint.sh /entrypoint.sh
RUN chown root:root /entrypoint.sh \
  && chmod 755 /entrypoint.sh

VOLUME $USER_HOME/.$SERVER_BIN
VOLUME $USER_HOME/.$CLI_BIN

WORKDIR /home/$TENDERMINT_USER

ARG START_CMD
ARG RPC_LADDR
ARG SEEDS
ARG PEERS

ENV TENDERMINT_USER=$TENDERMINT_USER \
    CHAIN_ID=$CHAIN_ID \
    SERVER_BIN=$SERVER_BIN \
    CLI_BIN=$CLI_BIN \
    START_CMD=$START_CMD \
    SEEDS=$SEEDS \
    PEERS=$PEERS \
    RPC_LADDR=$RPC_LADDR \
    USER_HOME=/home/$TENDERMINT_USER

VOLUME $USER_HOME/.$SERVER_BIN
VOLUME $USER_HOME/.$CLI_BIN

WORKDIR /home/$TENDERMINT_USER

HEALTHCHECK --interval=5s --timeout=3s CMD curl -sf http://localhost:26657/status

ENTRYPOINT [ "tini", "--", "/entrypoint.sh" ]
CMD [ "sh", "-c", "${START_CMD}" ]
