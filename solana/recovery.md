# Recovering to a new server

* Reset solana network

``` bash
sudo systemctl stop solana
rm -rf "${HOME}/validator-ledger"
```

* Recover keys and set network

``` bash
# keys
solana-keygen recover -o "${HOME}/validator-identity.json"
solana-keygen recover -o "${HOME}/validator-vote-account.json"
solana config set --keypair "${HOME}/validator-identity.json"
# url
solana config set --url http://testnet.solana.com # testnet
solana config set --url http://api.mainnet-beta.solana.com # mainnet-beta
```

* Edit Solana parameters to reflect current environment

``` bash
cd "${HOME}/stake-systems/ansible"
# edit the configuration
vi ./solana_tds.yml # use this for tds
vi ./solana_mainnet.yml # use this for mainnet
# apply configuration
ansible-playbook solana_tds.yml # use this for tds
ansible-playbook solana_mainnet.yml # use this for mainnet
```

* Check that everything is working

``` bash
sudo journalctl -u solana -f
solana --version && solana catchup ~/validator-identity.json http://127.0.0.1:8899/
```

* Cleanup - after you migrated back to your main server

``` bash
sudo systemctl stop solana
rm -rf "${HOME}/validator-ledger"
shred -u "${HOME}/validator-identity.json"
shred -u "${HOME}/validator-vote-account.json"
```
