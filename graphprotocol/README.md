# graphprotocol

docker-compose is wrapped in npm for a more streamlined experience  

Requirements: `docker.io docker-compose bash npm`

## DNS Configuration

``` text
graphprotocol.              IN  A       <PUBLIC_SERVER_IP>
adminer.graphprotocol.      IN  CNAME   graphprotocol.example.com.
grafana.graphprotocol.      IN  CNAME   graphprotocol.example.com.
prometheus.graphprotocol.   IN  CNAME   graphprotocol.example.com.
portainer.graphprotocol.    IN  CNAME   graphprotocol.example.com.
query.graphprotocol.        IN  CNAME   graphprotocol.example.com.
indexer.graphprotocol.      IN  CNAME   graphprotocol.example.com.
```

## Database copy - only if you are migrating

``` bash
docker volume create graphprotocol_postgres-graph
docker run --rm \
  -v <source_volume>:/from \
  -v graphprotocol_postgres-graph:/to \
  alpine ash -c "cp -rpv /from/* /to/"
```

## Running

* adjust your environment

``` bash
cp .env.local.sample .env.local
vi .env.local
```

``` bash
# bring up the stack - you can ctrl-c the log tail, stack is running in the background
# you run this every time you want to recreate everything
# this basically runs docker-compose down --remove-orphans and recreates the stack
npm run deploy

# refresh the stack - you run this every time you do a minor change to multiple services
npm run apply

# refresh a specific service - you run this every time you do a minor change to a single service
npm run apply indexer-agent

# tail logs for a service
npm run logs indexer-agent

# rebuild a service
npm run compose build -- --no-cache cli

# shell into cli container
npm run shell cli

# shell into indexer-agent container
npm run shell indexer-agent
```

* indexer operations

These operations are performed from the cli container  
`npm run shell cli`

``` bash

# phase 0 - do not run this if you already have the subgraphs deployed!
http post query-node-0:8020 jsonrpc="2.0" method="subgraph_create" id="2" params:='{"name": "synthetixio-team/synthetix"}'
http post query-node-0:8020 jsonrpc="2.0" id="2" method="subgraph_deploy" params:='{"name": "synthetixio-team/synthetix", "ipfs_hash": "Qme2hDXrkBpuXAYEuwGPAjr6zwiMZV4FHLLBa3BHzatBWx"}'

http post query-node-0:8020 jsonrpc="2.0" method="subgraph_create" id="2" params:='{"name": "uniswap/uniswap-v2"}'
http post query-node-0:8020 jsonrpc="2.0" id="2" method="subgraph_deploy" params:='{"name": "uniswap/uniswap-v2", "ipfs_hash": "QmXKwSEMirgWVn41nRzkT3hpUBw29cp619Gx58XW6mPhZP"}'

http post query-node-0:8020 jsonrpc="2.0" method="subgraph_create" id="1" params:='{"name": "molochventures/moloch"}'
http post query-node-0:8020 jsonrpc="2.0" id="1" method="subgraph_deploy" params:='{"name": "molochventures/moloch", "ipfs_hash": "QmTXzATwNfgGVukV1fX2T6xw9f6LAYRVWpsdXyRWzUR2H9"}'

http post query-node-0:8020 jsonrpc="2.0" method="subgraph_create" id="4" params:='{"name": "jannis/gravity"}'
http post query-node-0:8020 jsonrpc="2.0" id="4" method="subgraph_deploy" params:='{"name": "jannis/gravity", "ipfs_hash": "QmbeDC4G8iPAUJ6tRBu99vwyYkaSiFwtXWKwwYkoNphV4X"}'

# phase 1 - graph-cli operations
# graph indexer connect http://indexer-agent:8000
graph indexer status
graph indexer rules get all --merged
graph indexer rules always global

graph indexer rules set Qme2hDXrkBpuXAYEuwGPAjr6zwiMZV4FHLLBa3BHzatBWx allocationAmount 100
graph indexer rules set QmXKwSEMirgWVn41nRzkT3hpUBw29cp619Gx58XW6mPhZP allocationAmount 100
graph indexer rules set QmTXzATwNfgGVukV1fX2T6xw9f6LAYRVWpsdXyRWzUR2H9 allocationAmount 100
graph indexer rules set QmSXzWZhbDgDg9YyDeivFqYBc1zxESyBYBH3W9CzmupYQd allocationAmount 100

# rules will start the subgraphs on the last node, apparenty - index_node_1
graph indexer rules start Qme2hDXrkBpuXAYEuwGPAjr6zwiMZV4FHLLBa3BHzatBWx
graph indexer rules start QmXKwSEMirgWVn41nRzkT3hpUBw29cp619Gx58XW6mPhZP
graph indexer rules start QmTXzATwNfgGVukV1fX2T6xw9f6LAYRVWpsdXyRWzUR2H9
graph indexer rules start QmSXzWZhbDgDg9YyDeivFqYBc1zxESyBYBH3W9CzmupYQd
graph indexer rules get all --merged

graph indexer rules set global allocationAmount 0.1
graph indexer rules set QmTXzATwNfgGVukV1fX2T6xw9f6LAYRVWpsdXyRWzUR2H9 minStake 1000
graph indexer rules set QmTXzATwNfgGVukV1fX2T6xw9f6LAYRVWpsdXyRWzUR2H9 minStake 1000 maxSignal 500
graph indexer rules set global minAverageQueryFees 10000

# reasign uniswap-v2 subgraph to another node
http post query-node-0:8020 \
  jsonrpc="2.0" id="1" method="subgraph_reassign" \
  params:='{"name": "uniswap/uniswap-v2", "ipfs_hash": "QmXKwSEMirgWVn41nRzkT3hpUBw29cp619Gx58XW6mPhZP", "node_id": "index_node_0"}'
graph indexer rules start QmXKwSEMirgWVn41nRzkT3hpUBw29cp619Gx58XW6mPhZP

# the follwing command should work and show everyting as up
graph indexer status
```

## Using another graph database, if yours is corrupted

* from the working db docker host, you copy the database to the destination host

Before copying, make sure you've stopped the database on both servers  
Use screen command or put the transfer in the background  
The source server needs to be able to log in via passwordless SSH to the destination

``` bash
# source_volume: graphprotocol-infrastructure_postgres_data
# destination_volume: graphprotocol_postgres-graph
```

``` bash
# on the source host - check volume size, start the transfer
docker system df -v | grep graphprotocol-infrastructure_postgres_data | awk "{print \$3}"
docker run --rm -v graphprotocol-infrastructure_postgres_data:/from alpine ash -c \
  "cd /from && tar -cf - . " | ssh user@graphprotocol.stake.systems \
  'docker run --rm -i -v graphprotocol_postgres-graph:/to alpine ash -c "cd /to && tar -xpvf - "'
# on the destionation host - watch the progress
watch -n2 'docker system df -v | grep graphprotocol_postgres-graph | awk "{print \$3}"'
```
