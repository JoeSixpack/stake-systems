graph indexer status
graph indexer rules get all --merged
graph indexer rules always global
graph indexer rules set Qme2hDXrkBpuXAYEuwGPAjr6zwiMZV4FHLLBa3BHzatBWx allocationAmount 100
graph indexer rules set QmXKwSEMirgWVn41nRzkT3hpUBw29cp619Gx58XW6mPhZP allocationAmount 100
graph indexer rules set QmTXzATwNfgGVukV1fX2T6xw9f6LAYRVWpsdXyRWzUR2H9 allocationAmount 100
graph indexer rules set QmSXzWZhbDgDg9YyDeivFqYBc1zxESyBYBH3W9CzmupYQd allocationAmount 100
graph indexer rules start Qme2hDXrkBpuXAYEuwGPAjr6zwiMZV4FHLLBa3BHzatBWx
graph indexer rules start QmXKwSEMirgWVn41nRzkT3hpUBw29cp619Gx58XW6mPhZP
graph indexer rules start QmTXzATwNfgGVukV1fX2T6xw9f6LAYRVWpsdXyRWzUR2H9
graph indexer rules start QmSXzWZhbDgDg9YyDeivFqYBc1zxESyBYBH3W9CzmupYQd
graph indexer rules get all --merged
graph indexer rules set global allocationAmount 0.1
graph indexer rules set QmTXzATwNfgGVukV1fX2T6xw9f6LAYRVWpsdXyRWzUR2H9 minStake 1000
graph indexer rules set QmTXzATwNfgGVukV1fX2T6xw9f6LAYRVWpsdXyRWzUR2H9 minStake 1000 maxSignal 500
graph indexer rules set global minAverageQueryFees 10000
http post query-node-0:8020 jsonrpc="2.0" id="1" method="subgraph_reassign" params:='{"name": "uniswap/uniswap-v2", "ipfs_hash": "QmXKwSEMirgWVn41nRzkT3hpUBw29cp619Gx58XW6mPhZP", "node_id": "index_node_0"}'
graph indexer rules start QmXKwSEMirgWVn41nRzkT3hpUBw29cp619Gx58XW6mPhZP
graph indexer status