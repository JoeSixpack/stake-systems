#!/usr/bin/env bash
set -eEuo pipefail

cd "$(dirname "${0}")/.."
# shellcheck source=/dev/null
source ./.env.local
docker exec -it "${COMPOSE_PROJECT_NAME}_${1}" "${2:-/bin/bash}" || true
