#!/usr/bin/env bash
set -eEuo pipefail

cd "$(dirname "${0}")/.."
# shellcheck source=/dev/null
source ./.env.local
echo -n "${PORTAINER_PASSWORD}" > ./portainer-admin-password.txt
npm run down
docker-compose --env-file .env.local up -d
shred -u ./portainer-admin-password.txt
npm run logs
